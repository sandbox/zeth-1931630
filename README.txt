------------------------------------------------------------------------------
  Mozilla Open Badges Module for Drupal
------------------------------------------------------------------------------

Description
===========
The Mozilla Open Badges module hooks into the User Badges module to allow 
badges created by that module to be connected with the Mozilla Open Badges 
framework. It adds some Open Badge fields to the User Badges Module, and 
creates a badge assertion that can be passed to Mozilla Open Badges. It 
links to the Mozilla Open Badges javascript API, and includes a javascript 
file that interacts with it.

Changes/Additions to User Badges processes
==========
The mozilla_open_badges.install file adds two fields to the user_badges_badges 
table in the database, expiration (number of months before the badge expires) and 
description, to provide a badge description.

hook_form_alter is used to make the following changes to the User Badges Edit Form:
* Add Badge Expiration date and Badge Description fields to the form
* Remove the URL option so it's easier to test badge parameters
* Require the Criteria URL
* Force the mozilla open badges submission function first to get new fields into the database
* Add mozilla open badges form validation

mozilla_open_badges_user_badges_edit_form_validate adds the following validation to the form
* Requires expiration months to be numberic
* Requires that uploaded images are png and 90x90, per the Mozilla Open Badges specifications

theme_mozilla_user_badge_group re-themes the badge list, to allow for a link to the assertion

Dependencies
============
This is essentially a submodule, and relies on

* user_badges



Configuration
=============
Once the module is activated, go to Administer >> User management >> Mozilla Open Badges
(admin/user/mozilla_open_badges). Here you can set issuer and default values for badges.

Permissions
===========
The module does not affect permission. Users with 'access administration pages' 
can change configuration settings. 


Current maintainer
==================
Zeth Lietzau (http://drupal.org/user/866546)
